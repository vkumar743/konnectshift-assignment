// Sample codes Below

// 1. DB Connection
var mysql      = require('mysql');
var connection = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : '**********',
    database : 'KonnectShiftAssignment'
});
module.exports=connection;

// 2. Setting up nodejs server
var app = require('./app');
var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
    console.log('Express server listening on port ' + port);
    const all_routes = require('express-list-endpoints');
    console.log(all_routes(app));
});












// var server = require('express');
